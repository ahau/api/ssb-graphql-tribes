const typeDefs = require('./src/typeDefs')
const Resolvers = require('./src/resolvers')

module.exports = (ssb, gettersWithCache) => ({
  Context: {},
  typeDefs,
  resolvers: Resolvers(ssb, gettersWithCache)
})

const test = require('tape')
const { promisify: p } = require('util')
const clone = require('lodash.clonedeep')

const {
  Replicate,
  CreateGroupApplication,
  InitGroup,
  AcceptGroupApplication
} = require('../lib/helpers')

const TestBot = require('../test-bot')

function TestSetup (t) {
  return async function testSetup (_customFieldDefs, customFieldValues) {
    const customFieldDefs = clone(_customFieldDefs)
    const kaitiaki = await TestBot({ loadContext: true })
    const applicant = await TestBot({ loadContext: true })

    // kaitiaki helpers
    const initGroup = InitGroup(kaitiaki.ssb)
    const replicate = Replicate(kaitiaki.ssb)
    const acceptGroupApplication = AcceptGroupApplication(kaitiaki.apollo)

    // applicant helpers
    const createGroupApplication = CreateGroupApplication(applicant.apollo)

    // kaitiaki inits group with no custom fields
    const { groupId, poBoxId } = await initGroup({ preferredName: 'Eriepa Tribe', customFields: customFieldDefs })

    // replicate to the applicant
    await replicate(kaitiaki.ssb, applicant.ssb)

    // a community has no custom fields, and an applicant joins with no custom fields
    let res = await createGroupApplication(groupId, { comment: '', answers: [], customFields: customFieldValues })
    t.error(res.errors, 'applicant creates the application')

    // replicate to the kaitiaki
    await replicate(applicant.ssb, kaitiaki.ssb)

    const applicationId = res.data.createGroupApplication

    // accepting the application creates the profile correctly
    res = await acceptGroupApplication(applicationId, { comment: '', groupIntro: '' })
    t.error(res.errors, 'kaitiaki accepts the application')

    // replicate back to the applicant
    await replicate(kaitiaki.ssb, applicant.ssb)
    await new Promise(resolve => setTimeout(resolve, 500)) // sleep

    const profiles = await p(applicant.ssb.profile.findByFeedId)(applicant.ssb.id, { selfLinkOnly: false })

    // get the applicants group profile
    const groupProfile = profiles.other.private.find(profile => profile.recps[0] === groupId)

    // get the applicants admin profile
    const adminProfile = profiles.self.private.find(profile => profile.recps[0] === poBoxId)

    return { kaitiaki, applicant, profiles: { groupProfile, adminProfile } }
  }
}

// NOTE: each data field will use a timestamp as the key
const timestampA = generateTimestamp()
const timestampB = generateTimestamp()
const timestampC = generateTimestamp()

// NOTE: graphql will handle converting this to the appropriate format for ssb
const CUSTOM_FIELD_DEFS = [
  {
    key: timestampA,
    type: 'text',
    label: 'Hometown',
    order: 1,
    required: false,
    visibleBy: 'admin'
  },
  {
    key: timestampB,
    type: 'array',
    label: 'Qualifications',
    required: false,
    visibleBy: 'members'
  }
]

// - [ ] when there are no custom fields on the community, and someone joins with no custom fields
test('applicant joins with no custom fields', async t => {
  const testSetup = TestSetup(t)

  const customFieldDefs = []
  const customFieldValues = []

  const {
    kaitiaki,
    applicant,
    profiles: {
      groupProfile,
      adminProfile
    }
  } = await testSetup(customFieldDefs, customFieldValues)

  t.deepEqual(groupProfile.customFields, {}, 'no custom fields on the group profile')
  t.deepEqual(adminProfile.customFields, {}, 'no custom fields on the admin profile')

  kaitiaki.ssb.close()
  applicant.ssb.close()
  t.end()
})

function generateTimestamp () {
  return (
    Date.now() +
    Math.floor(Math.random() * 100)
  ).toString()
}

// - [ ] when there are custom fields on the community, and someone joins with no custom fields
test('applicant joins with no custom fields, when there are field defs', async t => {
  t.plan(4)
  const testSetup = TestSetup(t)

  const customFieldValues = []

  const {
    kaitiaki,
    applicant,
    profiles: {
      groupProfile,
      adminProfile
    }
  } = await testSetup(CUSTOM_FIELD_DEFS, customFieldValues)

  t.deepEqual(groupProfile.customFields, {}, 'no custom fields on the group profile')
  t.deepEqual(adminProfile.customFields, {}, 'no custom fields on the admin profile')

  kaitiaki.ssb.close()
  applicant.ssb.close()
})

// - [ ] when someone joins with admin custom fields and no member fields
test('applicant joins with admin custom fields and no member fields', async t => {
  t.plan(4)
  const testSetup = TestSetup(t)

  const customFieldValues = [
    {
      key: timestampA,
      value: 'Hamilton'
    }
  ]

  const {
    kaitiaki,
    applicant,
    profiles: {
      groupProfile,
      adminProfile
    }
  } = await testSetup(CUSTOM_FIELD_DEFS, customFieldValues)

  t.deepEqual(groupProfile.customFields, {}, 'no custom fields on the group profile')
  t.deepEqual(adminProfile.customFields, {
    [timestampA]: 'Hamilton'
  }, 'returns admin field on the admin profile')

  kaitiaki.ssb.close()
  applicant.ssb.close()
})

// - [ ] when someone joins with no admin fields and some member fields
test('applicant joins with no admin fields and some member fields', async t => {
  t.plan(4)
  const testSetup = TestSetup(t)

  const customFieldValues = [
    {
      key: timestampB,
      value: ['Bachelor of Science']
    }
  ]

  const {
    kaitiaki,
    applicant,
    profiles: {
      groupProfile,
      adminProfile
    }
  } = await testSetup(CUSTOM_FIELD_DEFS, customFieldValues)

  t.deepEqual(groupProfile.customFields, {
    [timestampB]: ['Bachelor of Science']
  }, 'group profile fields are on the group profile')

  t.deepEqual(adminProfile.customFields, {
    [timestampB]: ['Bachelor of Science']
  }, 'group profile fields are also on the admin profile')

  kaitiaki.ssb.close()
  applicant.ssb.close()
})

// - [ ] when someone joins with admin fields and member fields
test('applicant joins with admin fields and member fields', async t => {
  t.plan(4)
  const testSetup = TestSetup(t)

  const customFieldValues = [
    {
      key: timestampA,
      value: 'Hamilton'
    },
    {
      key: timestampB,
      value: ['Bachelor of Science']
    }
  ]

  const {
    kaitiaki,
    applicant,
    profiles: {
      groupProfile,
      adminProfile
    }
  } = await testSetup(CUSTOM_FIELD_DEFS, customFieldValues)

  t.deepEqual(groupProfile.customFields, {
    [timestampB]: ['Bachelor of Science']
  }, 'group profile fields are on the group profile')
  t.deepEqual(adminProfile.customFields, {
    [timestampA]: 'Hamilton',
    [timestampB]: ['Bachelor of Science']
  }, 'group profile fields are also on the admin profile')

  kaitiaki.ssb.close()
  applicant.ssb.close()
})

// - [ ] when someone joins with admin fields and member fields
test('applicant joins with admin fields and member fields (excludes extra fields)', async t => {
  t.plan(4)
  const testSetup = TestSetup(t)

  const customFieldValues = [
    {
      key: timestampA,
      value: 'Hamilton'
    },
    {
      key: timestampB,
      value: ['Bachelor of Science']
    },
    {
      key: timestampC,
      value: 'Something thats not supposed to be there'
    }
  ]

  const {
    kaitiaki,
    applicant,
    profiles: {
      groupProfile,
      adminProfile
    }
  } = await testSetup(CUSTOM_FIELD_DEFS, customFieldValues)

  t.deepEqual(groupProfile.customFields, {
    [timestampB]: ['Bachelor of Science']
  }, 'group profile fields are on the group profile')
  t.deepEqual(adminProfile.customFields, {
    [timestampA]: 'Hamilton',
    [timestampB]: ['Bachelor of Science']
  }, 'group profile fields are also on the admin profile')

  kaitiaki.ssb.close()
  applicant.ssb.close()
})

// - [ ] when someone joins with custom fields that arent on the community
test('applicant joins with custom fields that arent on the community', async t => {
  t.plan(4)
  const testSetup = TestSetup(t)

  const customFieldValues = [
    {
      key: timestampC,
      value: 'Cambridge'
    }
  ]

  const {
    kaitiaki,
    applicant,
    profiles: {
      groupProfile,
      adminProfile
    }
  } = await testSetup(CUSTOM_FIELD_DEFS, customFieldValues)

  t.deepEqual(groupProfile.customFields, {}, 'no custom fields on the group profile')
  t.deepEqual(adminProfile.customFields, {}, 'no custom fields on the admin profile')

  kaitiaki.ssb.close()
  applicant.ssb.close()
})

// need to cover special case with dates
// this was overlooked and has popped up as a bug in ahau. This test
// is just an extra measure
// NOTE: https://gitlab.com/ahau/lib/ssb-plugins/ssb-profile/-/blob/d75e4a0f129fb55979140f5ee391fc2d81294729/spec/lib/field-types.js#L110
test('applicant joins with custom fields that are of type date', async t => {
  t.plan(4)
  const testSetup = TestSetup(t)

  const customFieldDefs = [
    {
      key: timestampA,
      type: 'array',
      label: 'Qualifications',
      required: false,
      visibleBy: 'members'
    }
  ]

  const customFieldValues = [
    {
      key: timestampA,
      type: 'date',
      value: '2022-XX-XX' // edtf
    }
  ]

  const {
    kaitiaki,
    applicant,
    profiles: {
      groupProfile,
      adminProfile
    }
  } = await testSetup(customFieldDefs, customFieldValues)

  t.deepEqual(groupProfile.customFields, {
    [timestampA]: { type: 'date', value: '2022-XX-XX' }
  }, 'group profile has correctly formatted date field ')
  t.deepEqual(adminProfile.customFields, {
    [timestampA]: { type: 'date', value: '2022-XX-XX' }
  }, 'nadmin profile has correcrly formatted date field')

  kaitiaki.ssb.close()
  applicant.ssb.close()
})

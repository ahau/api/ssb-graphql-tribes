const { promisify: p } = require('util')
const testBot = require('../test-bot')
const { CreateGroupApplication, Replicate, InitGroup, Whoami } = require('../lib/helpers')

module.exports = async function setup () {
  const kaitiaki = await testBot({ loadContext: true, debug: false })
  const applicant = await testBot({ loadContext: true, debug: false })

  // kaitiaki helpers
  const replicate = Replicate(kaitiaki.ssb)
  const initGroup = InitGroup(kaitiaki.ssb)

  // applicant helpers
  const createGroupApplication = CreateGroupApplication(applicant.apollo)

  // simulate group (like in ssb-ahau)
  const { groupId, poBoxId } = await initGroup({
    joiningQuestions: [
      { label: 'what is your favourate colour?', type: 'input' }
    ]
  })

  // send this setup to applicant
  await replicate(kaitiaki.ssb, applicant.ssb)

  // set up a personal profile for applicant
  // the application process copies that to a kaitaiki only profile
  const whoami = await Whoami(applicant.apollo)

  const applicantPersonalProfileId = whoami.personal.profileId
  await p(applicant.ssb.profile.person.source.update)(applicantPersonalProfileId, {
    preferredName: 'tama',
    phone: '12345' // only present on source + admin profiles
  })

  const result = await createGroupApplication(groupId, {
    answers: [
      { question: 'what is your favourate colour?', answer: 'blue' }
    ],
    comment: 'Kia ora, thanks for giving me the opportunity to join!',
    customFields: []
  })

  const applicationId = result.data.createGroupApplication

  return {
    kaitiaki,
    applicant,

    groupId,
    poBoxId,
    applicationId
  }
}

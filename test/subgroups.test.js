const test = require('tape')
const { promisify: p } = require('util')
const testBot = require('./test-bot')

test('subGroups (create, get, list)', async t => {
  const { ssb, apollo } = await testBot()

  const { groupId } = await p(ssb.tribes.create)({})
  const group = { id: groupId }

  const getGroupResult = await apollo.query({
    query: `
      query($id: String!) {
        getGroup(id: $id) {
          id
          subGroups {
            id
            poBoxId
          }
        }
      }`,
    variables: {
      id: group.id
    }
  })

  t.error(getGroupResult.errors, 'getGroup query')
  t.deepEqual(
    getGroupResult.data.getGroup,
    {
      id: group.id,
      subGroups: []
    },
    'returns no subGroups yet'
  )

  // create a subGroup
  const createSubGroupRes = await apollo.mutate({
    mutation: `
      mutation($id: String!) {
        createSubGroup(id: $id) {
          id
          poBoxId
        }
      }
    `,
    variables: {
      id: group.id
    }
  })

  t.error(createSubGroupRes.errors, 'createSubGroup')

  const subGroup = createSubGroupRes.data.createSubGroup

  // create a second subGroup
  const createSubGroupRes2 = await apollo.mutate({
    mutation: `
      mutation($id: String!) {
        createSubGroup(id: $id) {
          id
          root
          # POBoxKey
        }
      }
    `,
    variables: {
      id: group.id
    }
  })

  t.error(createSubGroupRes.errors, 'createSubGroup')

  const subGroup2 = createSubGroupRes2.data.createSubGroup

  const getGroupResult2 = await apollo.query({
    query: `
      query($id: String!) {
        getGroup(id: $id) {
          id
          subGroups {
            id
          }
        }
      }`,
    variables: {
      id: group.id
    }
  })

  t.error(getGroupResult2.errors, 'getGroup2 query')
  t.deepEqual(
    getGroupResult2.data.getGroup,
    {
      id: group.id,
      subGroups: [
        { id: subGroup.id },
        { id: subGroup2.id }
      ]
    },
    'returns the two subGroups'
  )

  // list subGroups?
  const listRes = await apollo.query({
    query: `
      query($id: String!) {
        listSubGroups(id: $id) {
          id
        }
      }
    `,
    variables: {
      id: group.id
    }
  })

  t.error(listRes.errors, 'listSubGroups')

  t.deepEqual(
    listRes.data.listSubGroups,
    [
      { id: subGroup.id },
      { id: subGroup2.id }
    ],
    'returns the two subGroups as well'
  )

  ssb.close(t.end)
})

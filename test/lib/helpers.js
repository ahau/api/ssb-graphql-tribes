const { replicate } = require('scuttle-testbot')
const { promisify: p } = require('util')
const fixInput = require('@ssb-graphql/profile/src/lib/fix-input')
const omit = require('lodash.omit')

const GROUP_APPLICATION_FRAGMENT = `
  fragment GroupApplicationFields on GroupApplication {
    id
    group {
      id
      # TODO: add other details
    }
    applicantId
    applicant {
      id
      preferredName
    }
    decision {
      accepted
    }
    answers {
      question
      answer
    }
    history {
      type
      authorId
      author {
        id
        preferredName
        phone
      }
      timestamp
      ...on GroupApplicationCommentHistory {
        comment
      }
      ...on GroupApplicationAnswerHistory {
        answers {
          question
          answer
        }
      }
      ...on GroupApplicationDecisionHistory {
        decision {
          accepted
        }
      }
    }
  }
`
function Replicate (kaitiaki) {
  return async function (from, to) {
    await replicate({
      from,
      to,
      name: id => (id === kaitiaki.id) ? 'kaitiaki' : 'applicant',
      log: false
    })
  }
}

function ListApplications (apollo, t) {
  return async function listApplications (accepted) {
    const res = await apollo.query({
      query: `
        ${GROUP_APPLICATION_FRAGMENT}
        query($accepted: Boolean) {
          listGroupApplications(accepted: $accepted) {
            ...GroupApplicationFields 
          }
        }`,
      variables: {
        accepted
      }
    })

    if (res.errors) t.fail('lists applications without error')
    if (res.errors) console.log('pretty print >> ', JSON.stringify(res.errors, null, 2))

    return res.data.listGroupApplications
  }
}

function handleErr (err) {
  console.log(JSON.stringify(err, null, 2))
  return ({ errors: [err], err })
}

function SaveProfile (apollo, t) {
  return async function (input) {
    const res = await apollo.mutate({
      mutation: `
        mutation ($input: ProfileInput!) {
          saveProfile(input: $input)
        }
      `,
      variables: {
        input
      }
    })

    t.error(res.errors)

    return res.data.saveProfile // profileId
  }
}

function CreateGroupApplication (apollo) {
  return async function createGroupApplication (groupId, input) {
    return apollo.mutate({
      mutation: `
        mutation (
          $groupId: String!
          $answers: [GroupApplicationAnswerInput]
          $comment: String
          $customFields: [PersonGroupCustomFieldInput]
        ) {
          createGroupApplication(
            groupId: $groupId
            answers: $answers
            comment: $comment
            customFields: $customFields
          )
        }
      `,
      variables: {
        groupId,
        answers: input.answers,
        comment: input.comment,
        customFields: input.customFields
      }
    })
      .catch(handleErr)
  }
}

const privateOnlyFields = ['allowWhakapapaViews', 'allowStories', 'allowPersonsList']
const publicOnlyFields = ['joiningQuestions', 'customFields']

function InitGroup (ssb) {
  async function initGroupProfiles (groupId, input) {
    // auto set authors to the creator
    const details = {
      ...fixInput.community(input), // includes poBoxId
      authors: {
        add: [ssb.id]
      }
    }

    // // create the groups public profile
    const groupPublicProfileId = await p(ssb.profile.community.public.create)({ ...omit(details, privateOnlyFields), allowPublic: true })
    await p(ssb.profile.link.create)(groupPublicProfileId, { groupId, allowPublic: true })

    // create the groups private profile
    const groupProfileId = await p(ssb.profile.community.group.create)({ ...omit(details, publicOnlyFields), recps: [groupId] })
    await p(ssb.profile.link.create)(groupProfileId, { groupId })

    return { public: groupPublicProfileId, group: groupProfileId }
  }

  return async function initGroup (input) {
    const { groupId } = await p(ssb.tribes.create)({})
    // make a profile for you in this group

    let details = {
      authors: {
        add: [ssb.id]
      },
      recps: [groupId]
    }

    // init person group profile
    const groupProfileId = await p(ssb.profile.person.group.create)(details)
    await p(ssb.profile.link.create)(groupProfileId)

    // init admin subgroup
    const { poBoxId, groupId: adminGroupId } = await p(ssb.tribes.subtribe.create)(groupId, { addPOBox: true, admin: true })

    details = {
      authors: {
        add: [ssb.id, '*']
      },
      recps: [poBoxId, ssb.id]
    }

    // init person admin profile
    const adminProfileId = await p(ssb.profile.person.admin.create)(details)
    await p(ssb.profile.link.create)(adminProfileId)

    input.poBoxId = poBoxId

    const communityProfiles = await initGroupProfiles(groupId, input)

    return {
      groupId,
      adminGroupId: adminGroupId,
      poBoxId,
      community: {
        public: communityProfiles.public,
        group: communityProfiles.group
      },
      person: {
        group: groupProfileId,
        admin: adminProfileId
      }
    }
  }
}

function AcceptGroupApplication (apollo) {
  return async function (id, input) {
    return apollo.mutate({
      mutation: `
        mutation ($id: String!, $comment: String, $groupIntro: String) {
          acceptGroupApplication(id: $id, comment: $comment, groupIntro: $groupIntro)
        }`,
      variables: {
        id,
        comment: input.comment,
        groupIntro: input.groupIntro
      }
    })
  }
}

async function Whoami (apollo) {
  const result = await apollo.query({
    query: `{
      whoami {
        personal {
          groupId
          profileId
        }
        linkedProfileIds
      }
    }`
  })

  return result.data.whoami
}

module.exports = {
  ListApplications,
  Replicate,
  GROUP_APPLICATION_FRAGMENT,
  SaveProfile,
  CreateGroupApplication,
  InitGroup,
  AcceptGroupApplication,
  Whoami
}
